var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstvw",
  1: "i",
  2: "abcdefghiklmoprstvw",
  3: "m",
  4: "a",
  5: "dmnr",
  6: "imr"
};

var indexSectionNames =
{
  0: "all",
  1: "namespaces",
  2: "files",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Namespaces",
  2: "Files",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Pages"
};

